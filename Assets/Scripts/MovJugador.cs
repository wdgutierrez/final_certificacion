﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovJugador : MonoBehaviour {
	[SerializeField]
	Rigidbody rg;
	[SerializeField]
	float velocidad=2;
	[SerializeField]
	float VelocidadMax;
	Vector3 direccion;



	// Use this for initialization
	void Start () {
		rg = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		direccion = new Vector3(Input.GetAxis("Horizontal"),rg.velocity.y, Input.GetAxis("Vertical"));
		direccion *= velocidad;
		rg.velocity = new Vector3(direccion.x, rg.velocity.y, direccion.z);
	}
}
