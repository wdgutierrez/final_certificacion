﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyFollowPlayerVisionField : MonoBehaviour {

    public LayerMask IgnoreMask;
    public string estado;
    private NavMeshAgent agent;
    private Transform target;
    private SoundFXPlayer sonido;

    [SerializeField]
    private MusicController musica;

    [SerializeField]
    float TotalCoolDown=2;
    [SerializeField]
    float EstadoCoolDown=0;
    [SerializeField]
    float DistanciaDeVision;
    
    [SerializeField]
    Transform eye;
    Vector3 PosicionInicial;


    // Use this for initialization
    void Start()
    {
        estado = "buscando";
        agent = GetComponent<NavMeshAgent>();
        sonido = GetComponent<SoundFXPlayer>();
        PosicionInicial = transform.position;
    }

    void LookForPlayer() {
        Ray vision = new Ray(eye.position, eye.forward);
        RaycastHit hit;

        if (Physics.Raycast(vision, out hit, DistanciaDeVision))
        {
            Debug.DrawRay(vision.origin, vision.direction * DistanciaDeVision, Color.red);

            if (hit.collider.tag == "player")
            {
                Debug.Log("encontré algo "+hit.collider.name);
                sonido.PlayClip(0);
                if (musica!=null) {
                    musica.PlayCancion(1);
                }
                agent.SetDestination(hit.transform.position);
                estado = "persiguiendo";
                target = hit.transform;
                return;
            }
            else {
                agent.SetDestination(PosicionInicial);

            }
        }
        else {
            agent.SetDestination(PosicionInicial);

        }
        
    }
    void Perseguir() {
        agent.SetDestination(target.position);
        Ray vision = new Ray(eye.position, eye.forward);
        RaycastHit hit;

        if (Physics.Raycast(vision, out hit, DistanciaDeVision))
        {
            if (hit.collider.tag == "player")
            {
                agent.SetDestination(target.position);
                EstadoCoolDown = 0;
            }
            else {
                EstadoCoolDown += Time.deltaTime;
               // Debug.Log("cooldown andando: "+EstadoCoolDown);
                if (EstadoCoolDown >= TotalCoolDown)
                {
                    estado = "buscando";
                    musica.PlayCancion(0);

                }
            }
        }
        else {
            EstadoCoolDown += Time.deltaTime;
            //Debug.Log("cooldown andando: " + EstadoCoolDown);
            if (EstadoCoolDown >= TotalCoolDown)
            {
                estado = "buscando";
                agent.SetDestination(PosicionInicial);
                if (musica != null)
                {
                    Debug.Log("volviendo a musica ambiente");
                   musica.PlayCancion(0);
                }
            }
        }
    }
    void ConsultarEstado() {
        switch (estado) {
            case "buscando":
                LookForPlayer();
                break;
            case "persiguiendo":
                Perseguir();
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        ConsultarEstado();   
    }
}
