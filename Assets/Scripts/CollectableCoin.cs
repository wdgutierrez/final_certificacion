﻿using UnityEngine;
using System.Collections;

public class CollectableCoin : MonoBehaviour {

    [SerializeField]
    Rigidbody miRB;

    void Start() {
        miRB = GetComponent<Rigidbody>();
        miRB.angularVelocity=new Vector3(0,5,0);
    }

    void OnTriggerEnter(Collider col) {
        //play sound
        SendMessage("PlayClipOnce");
        //gameObject.SetActive(false);
    }	
}
