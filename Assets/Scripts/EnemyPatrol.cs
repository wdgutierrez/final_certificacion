﻿using UnityEngine;
using System.Collections;

public class EnemyPatrol : MonoBehaviour {
    public Transform[] points;
    private int destPoint = 0;
    private NavMeshAgent agent;
    public string estado;   
    public Transform target;
	public Transform targetJugador;
    private SoundFXPlayer sonido;
	public GameObject panelAtaque;

    [SerializeField]
    private MusicController musica;
    [SerializeField]
    Transform eye;
    [SerializeField]
    float TotalCoolDown = 2;
    [SerializeField]
    float EstadoCoolDown = 0;
    [SerializeField]
    float DistanciaDeVision;
	private Animator anim;
	public RectTransform barraSangre;


    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        sonido = GetComponent<SoundFXPlayer>();
		anim = GetComponent<Animator> ();
        agent.autoBraking = false;
        estado = "buscando";
    }

    void GotoNextPoint()
    {
        // Returns if no points have been set up
        if (points.Length == 0)
            return;

        // Set the agent to go to the currently selected destination.
        agent.destination = points[destPoint].position;

        // Choose the next point in the array as the destination,
        // cycling to the start if necessary.
        destPoint = (destPoint + 1) % points.Length;
        if (destPoint==points.Length-1) {
            destPoint = 0;
        }
    }

    void ConsultarEstado()
    {
        switch (estado)
        {
            case "buscando":
                // Choose the next destination point when the agent gets
                // close to the current one.
                if (agent.remainingDistance < 0.3f) {
                    GotoNextPoint();
                }
                LookForPlayer();

                break;
            case "persiguiendo":
                Perseguir();
                break;
        }
    }

    void LookForPlayer()
    {
        Ray vision = new Ray(eye.position, eye.forward);
        RaycastHit hit;

        if (Physics.Raycast(vision, out hit, DistanciaDeVision))
        {
            Debug.DrawRay(vision.origin, vision.direction * DistanciaDeVision, Color.red);

            if (hit.collider.tag == "player"){
                //Debug.Log("encontré algo " + hit.collider.name);
                sonido.PlayClip(0);
                if (musica != null)
                {
                    musica.PlayCancion(1);
                }
                agent.SetDestination(hit.transform.position);
                estado = "persiguiendo";
                target = hit.transform;
                return;
            }
            
        }

    }

	public void perseguir2(){
				agent.SetDestination(targetJugador.position);
	}
    public void Perseguir()
    {

        agent.SetDestination(target.position);
        Ray vision = new Ray(eye.position, eye.forward);
        RaycastHit hit;

        if (Physics.Raycast(vision, out hit, DistanciaDeVision))
        {
            if (hit.collider.tag == "player")
            {
                agent.SetDestination(target.position);


                //EstadoCoolDown = 0;
            }
//            else {
                //EstadoCoolDown += Time.deltaTime;
                // Debug.Log("cooldown andando: "+EstadoCoolDown);
//                if (EstadoCoolDown >= TotalCoolDown)
//                {
//                    estado = "buscando";
//                    musica.PlayCancion(0);
//
//                }
//            }
        }
//        else {
//            EstadoCoolDown += Time.deltaTime;
//            //Debug.Log("cooldown andando: " + EstadoCoolDown);
//            if (EstadoCoolDown >= TotalCoolDown)
//            {
//                estado = "buscando";
//                GotoNextPoint();
//                if (musica != null)
//                {
//                    Debug.Log("volviendo a musica ambiente");
//                    musica.PlayCancion(0);
//                }
//            }
//        }
    }

		public void alAtacar(){
				barraSangre.sizeDelta = new Vector2 (barraSangre.sizeDelta.x - 10, barraSangre.sizeDelta.y);
				panelAtaque.SetActive (true);
		}
    void Update()
    {
        ConsultarEstado();
				float dist = Vector3.Distance(targetJugador.position, transform.position);
				//print (dist);
				if (dist <= 3) {
						print ("llegooo");
						GetComponent<NavMeshAgent> ().speed = 0;
						anim.SetTrigger ("atacado");
						agent.SetDestination(target.position);
				} else  {
						print ("cae aqui");
						GetComponent<NavMeshAgent> ().speed = 2;
						panelAtaque.SetActive (false);
				}

    }
}
