﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(NavMeshAgent))]
public class EnemyFollowPlayer : MonoBehaviour {

    public NavMeshAgent agent;
    public Transform target;
	// Use this for initialization
	void Start () {
        agent = GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update () {
        if (target!=null) {
            agent.SetDestination(target.position);
        }
	}
}