﻿using UnityEngine;
using System.Collections;

public class SoundFXPlayer : MonoBehaviour {

    AudioSource source;

    [SerializeField]
    AudioClip[] clips;

	void Start () {
        source = GetComponent<AudioSource>();
	}

    public void PlayClip(int index) {
        source.PlayOneShot(clips[index]);
    }


}
