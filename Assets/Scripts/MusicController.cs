﻿using UnityEngine;
using System.Collections;

public class MusicController : MonoBehaviour {

    AudioSource source;

    [SerializeField]
    AudioClip[] clips;

    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    public void PlayCancion(int index)
    {
        source.clip = clips[index];
        source.Play();
    }
    public void PararCancion() {
        source.Stop();
        source.clip = null;
    }

}
