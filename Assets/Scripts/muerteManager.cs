﻿using UnityEngine;
using System.Collections;

public class muerteManager : MonoBehaviour {

		public RectTransform barraSangre;
		public GameObject shooty;
		public GameObject personaje;
		bool pausa = false ;
		public GameObject pausaCanvas;
        public GameObject canvasMuerte;

    //public CharacterController controlador;
    // Use this for initialization
    void Start () {
				Cursor.visible = false;
	}
	
	// Update is called once per frame
	void Update () {

				if (pausa == false) {
						Time.timeScale = 1;
						pausaCanvas.SetActive (false);
				} else {
						Time.timeScale = 0;
						pausaCanvas.SetActive (true);
				}

				if (barraSangre.sizeDelta.x <= 0) {
						Destroy (shooty);
						personaje.GetComponent<CharacterController> ().enabled = false;
            canvasMuerte.SetActive(true);

                }
				if (Input.GetKeyDown (KeyCode.Escape)) {
						//Time.timeScale = 0;
						pausa = !pausa;
				}
	}
}
