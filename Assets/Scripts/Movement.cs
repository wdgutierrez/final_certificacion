﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {
	[SerializeField]
	Rigidbody       miRB;
	[SerializeField]
	float           velocidad=2;
	[SerializeField]
	float           VelocidadMax;
	// Use this for initialization
	void Start () {
		miRB = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 direccion= new Vector3(Input.GetAxis("Horizontal"),miRB.velocity.y, Input.GetAxis("Vertical"));
		direccion *= velocidad;
		miRB.velocity = new Vector3(direccion.x, miRB.velocity.y, direccion.z);
	}
}
