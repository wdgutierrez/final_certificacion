﻿using UnityEngine;
using System.Collections;

public class PlaysSound : MonoBehaviour {
    [SerializeField]
    AudioSource source;
    [SerializeField]
    AudioClip   clip;

    public void Start() {
        source = GetComponent<AudioSource>();
    }

    public void PlayClipOnce() {
        source.PlayOneShot(clip);
    }
}
