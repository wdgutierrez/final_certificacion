﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {
    [SerializeField]
    Transform Objetivo;
    [SerializeField]
    float Distancia;
    // Use this for initialization
    void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(Objetivo.position.x, Objetivo.position.y+1, Objetivo.position.z-Distancia);
	}
}
