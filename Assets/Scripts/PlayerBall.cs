﻿using UnityEngine;
using System.Collections;

public class PlayerBall : MonoBehaviour {
    [SerializeField]
    Rigidbody       miRB;
    [SerializeField]
    float           velocidad=2;
    [SerializeField]
    float           VelocidadMax;
    [SerializeField]
    float           FuerzaSalto=5;
    [SerializeField]
    bool TocandoPiso;

    void Start() {
        miRB = GetComponent<Rigidbody>();
    }

    void OnCollisionEnter(Collision col) {
        if (col.gameObject.tag=="piso") {
            TocandoPiso = true;
        }
    }
    void OnCollisionExit(Collision col) {
        if (col.gameObject.tag=="piso") {
            TocandoPiso = false;
        }
    }

    void Update() {
        Vector3 direccion= new Vector3(Input.GetAxis("Horizontal"),miRB.velocity.y, Input.GetAxis("Vertical"));
        direccion *= velocidad;
        miRB.velocity = new Vector3(direccion.x, miRB.velocity.y, direccion.z);
        if (TocandoPiso) {
            if (Input.GetAxis("Jump")>0) {
                miRB.AddForce(0,1*FuerzaSalto,0);
                Debug.Log("saltando");
            }
        }
    }

}
