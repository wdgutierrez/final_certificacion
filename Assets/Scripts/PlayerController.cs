﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
    [SerializeField]
    Rigidbody miRB;
    [SerializeField]
    float velocidad = 2;
    [SerializeField]
    float FuerzaSalto = 5;
    [SerializeField]
    float velocidadRotacion = 4;
    [SerializeField]
    bool TocandoPiso;
    [SerializeField]
    Animator miAnimator;

    void Start()
    {
        miRB = GetComponent<Rigidbody>();
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "piso")
        {
            TocandoPiso = true;
        }
    }
    void OnCollisionExit(Collision col)
    {
        if (col.gameObject.tag == "piso")
        {
            TocandoPiso = false;
        }
    }

    void Update()
    {
        float inputH = Input.GetAxis("Horizontal");
        float inputV = Input.GetAxis("Vertical");
        miRB.angularVelocity = Vector3.zero;
        //CONTROL DE ANIMACIÓN
        miAnimator.SetFloat("Lados", Input.GetAxis("Horizontal"));
        miAnimator.SetFloat("Frente", Input.GetAxis("Vertical"));

        Vector3 direccion = new Vector3(0,miRB.velocity.y,0);

        if (inputH  !=   0)
            direccion += transform.right * velocidad*inputH;
        if (inputV   !=  0)
            direccion += transform.forward * velocidad*inputV;

        miRB.velocity = direccion;
        //CONTROL DE SALTO
        if (TocandoPiso)
        {
            if (Input.GetAxis("Jump") > 0)
            {
                miRB.AddForce(0, 1 * FuerzaSalto, 0);
                Debug.Log("saltando");
            }
        }
        //ROTACIÓN CON EL MOUSE
        transform.Rotate(new Vector3(0,1,0)* velocidadRotacion*Input.GetAxis("Mouse X")) ;


    }
}
