﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class sangreScript : MonoBehaviour {

		public const int maxHealth = 100;
		public int currentHealth = maxHealth;
		public RectTransform barraSangre;
		private Animator anim;
		public EnemyPatrol patrulla;
		public int dureza;
		public Text contadorMuertos;
		//public Animator animCofre;
		//public AudioSource[] AudioClips = null;
		//public AudioSource golpe;
		//public AudioSource destroyed;
		//public GameObject ariete;
		//public ParticleSystem luzy;


		// Use this for initialization

		void OnTriggerEnter(Collider other)
		{
				//Debug.Log("Objeto entra en trigger");
				//currentHealth -= 10;
				if (other.gameObject.tag == "bala") {
						barraSangre.sizeDelta = new Vector2 (barraSangre.sizeDelta.x - dureza, barraSangre.sizeDelta.y);

						//anim.SetTrigger("atacado");
						patrulla.perseguir2 ();
				}
				//AudioClips[0].Play();
				//golpe.Play ();
		}

		public void desaparece(){
				gameObject.GetComponent<BoxCollider> ().enabled = false;
				barraSangre.parent.gameObject.SetActive (false); 
				Destroy (gameObject);
		}

		void OnTriggerStay(Collider other)
		{
				// Debug.Log("Objeto esta en trigger");
		}
		void OnTriggerExit(Collider other)
		{
				//Debug.Log("Sale de trigger");
		}


		void Start () {
				anim = GetComponent<Animator> ();
		}

		// Update is called once per frame
		void Update () {
				//print (barraSangre.sizeDelta);
				if (barraSangre.sizeDelta.x <= 0) {


						//animCofre.Play ("Open [0]");
//						luzy.Play ();
//
//						if (AudioClips [0].isPlaying) {
//								AudioClips [0].Stop ();
//								AudioClips [1].Play ();
//						} 


						anim.SetTrigger("morite");
						GetComponent<NavMeshAgent> ().speed = 0;

						//Destroy(ariete);
						//Destroy(gameObject);

				
				} 
		}
}

